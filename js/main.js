$(document).ready(function() {
  $(window).load(function() {
    $("#content-loader").animate(
    {
      opacity: 0
    },
    {
      duration: 1000,
      queue: true,
      complete: function() {
        $(this).hide();
      }
    });
  });

  $("#start-view").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);

    $("#part0").animate(
    {
      opacity: 0
    },
    {
      duration: 1000,
      queue: false,
      complete: function() {
        $(this).hide();
      }
    });

    $("#part1").animate(
    {
      opacity: 1
    },
    {
      duration: 2000,
      queue: false
    });
  });

  $(".to-start").click(function() {
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 2000);

    $("#part1").animate(
    {
      opacity: 0
    },
    {
      duration: 1000,
      queue: false
    });

    $("#part0").css("display", "block").animate(
    {
      opacity: 1
    },
    {
      duration: 2000,
      queue: false
    });
  });

  $(".fancybox").fancybox({
    closeBtn  : false,
    arrows    : true,
    nextClick : false,
    mouseWheel: true,

    tpl: {
      error: '<p class="fancybox-error">Невозможно загрузить изображение.<br/>Повторите попытку позже.<br/>Если ошибка будет повторяться,<br/>вы можете написать на <a href="mailto:zvorygin@yararchive.ru?subject=Проблемы с показом изображений в электронной выставке Страницы мужества: Помощь Ярославской области блокадному Ленинграду. 1941—1944 гг.">zvorygin@yararchive.ru</a></p>'
    },

    helpers : {
      thumbs : {
        width  : 50,
        height : 50
      }
    }
  });

      
});